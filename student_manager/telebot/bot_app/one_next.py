from unittest.mock import call
from aiogram.utils.callback_data import CallbackData

from aiogram import types
from aiogram.dispatcher import FSMContext

from . import messages
from .app import dp, bot
from .keyboards import inline_kb, buttons, kb2, kb1, kb
from .states import GameStates
from .data_fetcher import get_nex



@dp.message_handler(commands='TEST', state="*")
async def train_all(message: types.Message, state: FSMContext):
    await GameStates.all_words.set()
    res = await get_nex(0)
    if not res:
        await GameStates.start.set()
        await message.reply('All !')
        return
    async with state.proxy() as data:
        data['step'] = 1
        data['pk'] = 1
        #data['pk'] = res.get('pk')
        data['answer'] = res.get('answers')
        data['question'] = res.get('question')
        await message.reply(f'{data["step"]}. {data["question"]}', reply_markup=inline_kb)

@dp.callback_query_handler(lambda c: c.data in ['da', 'di', 'de'], state=GameStates.all_words)
async def button_click_back_all(callback_query: types.CallbackQuery, state:FSMContext):
    await bot.answer_callback_query(callback_query.id)
    answer = callback_query.data
    async with state.proxy() as data:
        if answer == data.get('answer'):
            await bot.send_message(callback_query.from_user.id, 'Yes')
            res = await get_nex(data.get('pk'))
            if res:
                data['step'] +=1
                data['answer'] = res.get('answers')
                data['question'] = res.get('question')
                data['pk'] = res.get('pk')
                await bot.send_message(callback_query.from_user.id, f'{data["step"]}. {data["question"]}', reply_markup=inline_kb)
            else:
                await bot.send_message(callback_query.from_user.id, text='Яка загальна кількість балів?\n',
                                       reply_markup=buttons)
                #await GameStates.start.set()

        else:
            res = await get_nex(data.get('pk'))
            if res:
                data['step'] += 1
                data['answer'] = res.get('answers')
                data['question'] = res.get('question')
                data['pk'] = res.get('pk')
                await bot.send_message(callback_query.from_user.id, f'{data["step"]}. {data["question"]}',
                                               reply_markup=inline_kb)
            else:
                await bot.send_message(callback_query.from_user.id, text='Яка загальна кількість балів?\n',
                                       reply_markup=buttons)
                #await GameStates.random_ten.set()



@dp.callback_query_handler(text='one')
async def callback_one(callback: types.CallbackQuery):
    await callback.message.answer(f'Рекомендую навчальний заклад\n СДПУ імені А.С. Макаренка\n',
                                          reply_markup=kb)


@dp.callback_query_handler(text='two')
async def callback_two(callback: types.CallbackQuery):
    await callback.message.answer(f"Рекомендую навчальний заклад\n Національний університет «Львівська політехніка»\n",
                                          reply_markup=kb1 )


@dp.callback_query_handler(text='three')
async def callback_three(callback: types.CallbackQuery):
    await callback.message.answer(f"Рекомендую навчальний заклад\n Київський національний університет імені Тараса Шевченка\n",
                                   reply_markup=kb2)


