from aiogram import types
from .app import dp
from . import messages

from aiogram.utils.exceptions import BotBlocked


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    await message.reply(messages.WELCOME_MESSAGE)
    await message.delete()


@dp.errors_handler(exception=BotBlocked)
async def error_bot_block(update: types.Update, exception: BotBlocked):
    print('Нас заблокували!!!!')
    return True


