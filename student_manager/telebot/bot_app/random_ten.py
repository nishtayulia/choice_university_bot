from aiogram import types
from aiogram.dispatcher import FSMContext
from .app import dp, bot
from .keyboards import inline_kb2, kbut
from .states import GameStates
from .data_fetcher import get_random
from . import messages
import random
from .local_settings import random_pty

@dp.message_handler(commands='NO_TEST', state="*")
async def train_all(message: types.Message, state: FSMContext):
    await GameStates.random_ten.set()
    res = await get_random(0)
    if not res:
        await GameStates.start.set()
        await message.reply('All is done!')
        return
    async with state.proxy() as data:
        data['step'] = 1
        data['pk'] = 1
        data['answer2'] = res.get('answers2')
        data['question2'] = res.get('question2')
        await message.reply(f'{data["step"]}. {data["question2"]}', reply_markup=inline_kb2)
    await message.delete()

@dp.callback_query_handler(lambda c: c.data in ['ta', 'ti', 'te'], state=GameStates.random_ten)
async def button_click_back_all(callback_query: types.CallbackQuery, state:FSMContext):
    await bot.answer_callback_query(callback_query.id)
    answer = callback_query.data
    async with state.proxy() as data:
        if answer == data.get('answer2'):
            await bot.send_message(callback_query.from_user.id, 'Yes')
            res = await get_random(data.get('pk'))
            if res:
                data['step'] += 1
                data['answer2'] = res.get('answers2')
                data['question2'] = res.get('question2')
                data['pk'] = res.get('pk')
                url = random.choice(random_pty)
                await bot.send_message(callback_query.from_user.id, f"Рекомендую навчальний заклад\n {url} ")
                await GameStates.start.set()

        else:
            await bot.send_message(callback_query.from_user.id, f'Бажаю успіхів. \n', reply_markup=kbut)
            await bot.send_photo(callback_query.from_user.id, photo='https://cdn.pixabay.com/photo/2017/05/28/22/48/smile-2352472_1280.png')
            await GameStates.start.set()


