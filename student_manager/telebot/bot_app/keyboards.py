from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

inline_button_de = InlineKeyboardButton('Українська мова', callback_data='de')
inline_button_di = InlineKeyboardButton('Математика', callback_data='di')
inline_button_da = InlineKeyboardButton('Історія', callback_data='da')
inline_kb = InlineKeyboardMarkup()

inline_kb.add(inline_button_de)
inline_kb.add(inline_button_di)
inline_kb.add(inline_button_da)


buttons = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton('100-140 балів', callback_data='one')],
        [InlineKeyboardButton('140-180 балів', callback_data='two')],
        [InlineKeyboardButton('180-200 балів', callback_data='three')],
    ])


inline_button_te = InlineKeyboardButton('Планую наступного року', callback_data='te')
inline_button_ti = InlineKeyboardButton('Вони мені не потрібні', callback_data='ti')
inline_button_ta = InlineKeyboardButton('Де можна навчатися без ЗНО?', callback_data='ta')
inline_kb2 = InlineKeyboardMarkup()

inline_kb2.add(inline_button_te)
inline_kb2.add(inline_button_ti)
inline_kb2.add(inline_button_ta)


#university
kb = InlineKeyboardMarkup(resize_keyboard=True)
b = InlineKeyboardButton(text='university', url='https://www.sspu.edu.ua/' )
but = InlineKeyboardButton(text='Ranking of universities', url='https://osvita.ua/vnz/rating/86578/' )
kb.add(b).add(but)

kb1 = InlineKeyboardMarkup(resize_keyboard=True)
b1 = InlineKeyboardButton(text='university', url='https://lpnu.ua/' )
but = InlineKeyboardButton(text='Ranking of universities', url='https://osvita.ua/vnz/rating/86578/' )
kb1.add(b1).add(but)

kb2 = InlineKeyboardMarkup(resize_keyboard=True)
b2 = InlineKeyboardButton(text='university', url='http://www.univ.kiev.ua/' )
but = InlineKeyboardButton(text='Ranking of universities', url='https://osvita.ua/vnz/rating/86578/' )
kb2.add(b2).add(but)

kbut = InlineKeyboardMarkup(resize_keyboard=True)
butt = InlineKeyboardButton(text='Work', url='https://www.work.ua/' )
kbut.add(butt)
