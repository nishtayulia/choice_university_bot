from aiogram import executor
from bot_app import dp

async def on_startup(_):
    print('Hello, my friend!')


if __name__=='__main__':
    executor.start_polling(dp, skip_updates=True,
                           on_startup=on_startup)
