from django.db import models

# Create your models here.
class Profile(models.Model):
    external_id = models.PositiveIntegerField(
        verbose_name='ID user in Telegram bot',
        unique=True,
    )
    name = models.TextField(
        verbose_name='Name user',
    )

    def __str__(self):
        return f'#{self.external_id} {self.name}'

    class Meta:
        verbose_name = "Profil"
        verbose_name_plural = 'Profils'


class Message(models.Model):
    profile = models.ForeignKey(
        to='profile_user.Profile',
        verbose_name='Profil',
        on_delete=models.PROTECT,
    )
    text = models.TextField(
        verbose_name='Text',
    )
    created_at = models.DateTimeField(
        verbose_name='Time new message ',
        auto_now_add=True,
    )

    def __str__(self):
        return f'Message {self.pk} in {self.profile}'

    class Meta:
        verbose_name = 'Message'
