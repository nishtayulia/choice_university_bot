import random
from django.shortcuts import render
from rest_framework import serializers

from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponseNotFound
from . import models


class WordSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Question
        fields = ['pk', 'answers', 'question']


class WordSerializator2(serializers.ModelSerializer):
    class Meta:
        model = models.Question2
        fields = ['pk', 'answers2', 'question2']


class RandomWord(APIView):
    def get(self, *args, **kwargs):
        all_words = models.Question2.objects.all()
        random_word = random.choice(all_words)
        serialized_random_word = WordSerializator2(random_word, many=False)
        return Response(serialized_random_word.data)


class NextWord(APIView):
    def get(self, request, pk, format=None):
        question = models.Question.objects.filter(pk__gt=pk).first()
        if not question:
            return HttpResponseNotFound
        ser_word = WordSerializator(question, many=False)
        return Response(ser_word.data)

