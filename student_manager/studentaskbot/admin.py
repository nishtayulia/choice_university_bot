from django.contrib import admin

from . import models


class WordAdmin(admin.ModelAdmin):
    list_display = ['pk', 'answers', 'question']
    list_editable = ['answers', 'question']


admin.site.register(models.Question, WordAdmin)

class QuestionAdmin(admin.ModelAdmin):
    list_display = ['pk', 'answers2', 'question2']
    list_editable = [ 'answers2', 'question2']

admin.site.register(models.Question2, QuestionAdmin)


