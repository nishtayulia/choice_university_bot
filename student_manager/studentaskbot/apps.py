from django.apps import AppConfig


class StudentaskbotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'studentaskbot'
