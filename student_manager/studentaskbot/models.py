from django.db import models


class Question(models.Model):
    ANSWERS = [
        ('de', 'Українська мова'),
        ('di', 'Математика'),
        ('da', 'Історія'),
    ]

    question = models.CharField(verbose_name='Question', max_length=100)
    answers = models.CharField(verbose_name='Answers', max_length=3, choices=ANSWERS)

    def __str__(self):
        return self.answers+ ' '+ self.question


class Question2(models.Model):
    ANSWERS_PLAN = [
        ('te', 'Планую наступного року'),
        ('ti', 'Вони мені не потрібні'),
        ('ta', 'Де можна навчатися без ЗНО?'),
    ]

    question2 = models.CharField(verbose_name='Question2', max_length=100)
    answers2 = models.CharField(verbose_name='Answers2', max_length=3, choices=ANSWERS_PLAN)

    def __str__(self):
        return self.answers2+ ' '+ self.question2


