# choice_university_bot


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nishtayulia/choice_university_bot.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/nishtayulia/choice_university_bot/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
Сhoice university Telegrambot

## Description
Головною метою створення бота є допомога, майбутнім студентам з вибором навчального закладу згідно критеріїв, що висуне заклад освіти. 
Телеграм бот повинен провести опитування користувача, та відповідно отриманих результатів рекомендувати підходящий навчальний заклад, якщо набрано 
потрібну кількість балів, та ЗНО здано з відповідних предметів рекомендувати вищий навчальний заклад, якщо не задоволяють результати опитування
рекомендувати вступ до техучилища, або порекомендувати сайт з пошуку роботи. 
Для телеграм бота створено адмінпанель, що дає можливість змінювати запитання, їх послідовність та кількість, відповідно до структури опитування.
Адмінпанель, має функцію, збереження інформації про користувача, а саме: номер телефону, ім'я та повідомлення, що містить результати  
виборів користувача. 



## Project status
Проект знаходиться на етапі розробки та вдосконалення.